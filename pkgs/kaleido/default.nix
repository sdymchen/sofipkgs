{ python3Packages, fetchurl }:

python3Packages.buildPythonPackage rec {
    pname = "kaleido";
    version = "0.2.0";
    format = "wheel";
    src = fetchurl {
      url = "https://github.com/plotly/Kaleido/releases/download/v0.2.0/kaleido-0.2.0-py2.py3-none-manylinux1_x86_64.whl";
      sha256 = "sha256-nbF2JfXGrkYAdiuXwdgpbWe+IPNKiFTr5fsmSste7Zc=";
    };
    doCheck = false;
  }
