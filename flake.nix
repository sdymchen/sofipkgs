{
  description = "A very basic flake";

  inputs = {
     nixpkgs.url = "github:nixos/nixpkgs/22.11";
  };

  outputs = { self, nixpkgs }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
  in
  {
    packages.${system} = rec {
      nflows = pkgs.callPackage ./pkgs/nflows { };
      sbi = pkgs.callPackage ./pkgs/sbi { inherit pyknos; };
      pyknos = pkgs.callPackage ./pkgs/pyknos { inherit nflows; };
      kaleido = pkgs.callPackage ./pkgs/kaleido { };
    };

  };
}
