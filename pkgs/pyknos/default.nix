{ python3Packages, nflows }:

python3Packages.buildPythonPackage rec {
    pname = "pyknos";
    version = "0.14.2";
    src = python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-pTs6KHeHO9ohEZfSKhjw5KTBwcWuy/1a0jIpkhNFFuo=";
    };
    propagatedBuildInputs = with python3Packages; [
      matplotlib
      numpy
      tensorboard
      pytorch # ?
      tqdm
    ] ++ [
      nflows
    ];
    doCheck = false;
  }

