{ python3Packages, pyknos }:

python3Packages.buildPythonPackage rec {
    pname = "sbi";
    version = "0.19.0";
    src = python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-lBmMje41yaQwBCaI/9uwD+K19Htys9zPCMsTZ0bYOdI=";
    };
    propagatedBuildInputs = with python3Packages; [
      arviz
      joblib
      matplotlib
      numpy
      pillow
      pyro-ppl
      scikit-learn
      scipy
      tensorboard
      pytorch # ?
      tqdm
    ] ++ [
      pyknos
    ];
    doCheck = false;
  }
