{ python3Packages }:

python3Packages.buildPythonPackage rec {
    pname = "nflows";
    version = "0.14";
    src = python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-YpmESmL5mZ/N8tlcstAcCRpQE2vReCbjA6umRrLRG1U=";
    };
    propagatedBuildInputs = with python3Packages; [
      matplotlib
      numpy
      tensorboard
      pytorch # ?
      tqdm
    ];
    doCheck = true;
  }

